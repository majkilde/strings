# @majkilde/strings


## Install

```
$ npm install @majkilde/strings
```

## Usage

```js
import Strings from './strings';

Strings.strRight("Hello World!", " ");
//=> "World!"
```