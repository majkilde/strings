/**
 * Works on both Strings and String Arrays (like string @functions in Lotus Notes)
 */
let Strings = {
    /**
     *
     * @param value
     * @returns {boolean} True if value is a String
     */
    "isString": function (value) {
        return typeof (value) === 'string' || value instanceof String;
    },

    /**
     * @param value
     * @returns {boolean} true if value is an array
     */
    "isArray": function (value) {
        if (value == undefined) {
            return false;
        }
        return (value.constructor === Array);
    },
    /**
     *
     * @param value String or String[]
     * @returns a String Array
     */
    "toArray": function (value) {
        if (Strings.isArray(value)) {
            return value;
        }
        let result = [];
        if (value) {
            result.push(value);
        }
        return result;
    },
    /**
     *
     * @param value
     * @returns true if value is: undefinded, "", [], [""]
     */
    "isEmpty": function (value) {
        if (value == undefined)
            return true;
        let arr = Strings.toArray(value);
        if (arr.length == 0) return true;
        if (arr.length == 1 && (arr[0] == undefined || arr[0] == "")) return true;

        return false;
    },

    /**
     * String: remove leading/trailing spaces
     * Array: remove empty elements
     * @param value String or String Array
     * @returns {Array}
     */
    "trim": function (value) {

        if (Strings.isArray(value)) {
            let newArray = [];
            for (let i = 0; i < value.length; i++) {
                if (value[i]) {
                    newArray.push(value[i]);
                }
            }
            return newArray;
        } else {
            return value.trim();
        }

    },
    /**
     * Removes empty entries and trims leading/trailing whitespace
     * @param value
     * @returns {Array} trimed array
     */
    "fulltrim": function (value) {

        if (Strings.isArray(value)) {
            let newArray = [];
            for (let i = 0; i < value.length; i++) {
                if (value[i]) {
                    newArray.push(value[i].trim());
                }
            }
            return newArray;
        } else {
            value.trim();
        }
    },

    "escapeRegExp": function (str) {
        return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
    },

    /**
     *
     * @param value
     * @param find
     * @param replace
     * @returns {*}
     */
    "replaceAll": function (value, find, replace) {
        if (value) {
            return value.replace(new RegExp(Strings.escapeRegExp(find), 'g'), replace);
        }
        return value;
    },
    /**
     *
     * @param value
     * @returns Array with unqiue elements (any duplets are removed)
     */
    "unique": function (value) {
        if (Strings.isArray(value)) {
            return value.filter(function (value, index, self) {
                return self.indexOf(value) === index;
            });
        }
        return Strings.toArray(value);
    },
    /**
     *
     * @param value
     * @returns {Array} duplets and empty values are removed
     */
    "uniqueTrim": function (value) {
        let result = Strings.trim(Strings.unique(value));
        return result;
    },

    "uniqueTrimSort": function (value) {
        let result = Strings.trim(Strings.unique(value));
        return result.sort();
    },

    "strLeft": function (value, searchString) {
        if (value) {
            let pos = value.indexOf(searchString);
            if (pos >= 0) {
                return value.substring(0, pos);
            }
        }
        return value;
    },
    "strLeftBack": function (value, searchString) {
        if (value) {
            let pos = value.lastIndexOf(searchString);
            if (pos >= 0) {
                return value.substring(0, pos);
            }
        }
        return value;
    },

    "strRight": function (value, searchString) {
        if (value) {
            let pos = value.indexOf(searchString);
            if (pos >= 0) {
                return value.substring(pos + searchString.length);
            }
        }
        return value;
    },

    "strRightBack": function (value, searchString) {
        if (value) {
            let pos = value.lastIndexOf(searchString);
            if (pos >= 0) {
                return value.substring(pos + searchString.length);
            }
        }
        return value;
    },

    "strToken": function (value, delimiter, index) {
        let arr = (value || "").split(delimiter || ',');

        let pos = index || 1; //defaults to 1 (first token)
        if (pos == 0) pos = 1; //1=first token - and same as 0

        if (Math.abs(pos) > arr.length) return ""; //out of bounds

        if (pos < 0) {
            //count from back
            return arr[arr.length + pos];
        } else {
            return arr[pos - 1];
        }
    },

    'startsWith': function (string, prefix) {
        return string.lastIndexOf(prefix, 0) === 0;
    },

    'contains': function (string, search) {
        return string.indexOf(search) >= 0;
    },

    "getHtmlContent": function (value) {
        let result = Strings.strLeft(value, "</body");
        result = Strings.strRight(result, "<body");
        result = Strings.strRight(result, ">");
        result = result.replace(/(?:\r\n|\r|\n)/g, '<br/>');
        return result;
    },
    /**
     *
     * @param value
     * @return first letter in each word - in uppercase
     */
    "getInitials": function (value, minimunLength) {
        if (!value) return "";
        let words = value.trim().split(' ');
        let result = "";

        let cnt = (words.length < minimunLength) ? 2 : 1;
        $.each(words, function () {
            result += this.substring(0, cnt).toUpperCase();
        });

        if (result.length < minimunLength) {
            return
        }
        return result;
    },
    /**
     *
     * Sample:
     * list = [ {value:'north', key:'N'}, {value:'south', key:'S'}, {value:'east', key:'E'}, {value:'west', key:'W'}  ]
     * Strings.pluck( list, 'key')
     * => ['N', 'S', 'E', 'W']
     */
    "pluck": function (list, propertyName) {
        var result = [];
        for (var i = 0; i < list.length; ++i)
            result.push(list[i][propertyName]);
        return result;
    }
};

export default Strings